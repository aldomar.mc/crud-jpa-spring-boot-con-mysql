package com.zeytol.jpa_crud.repositories;

import com.zeytol.jpa_crud.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {
}

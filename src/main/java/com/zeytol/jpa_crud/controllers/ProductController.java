package com.zeytol.jpa_crud.controllers;

import com.zeytol.jpa_crud.models.Product;
import com.zeytol.jpa_crud.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping("/list")
    public String home(Model model) {
        List<Product> productList = productService.getAll();
        model.addAttribute("productList", productList);
        return "home";
    }

    @GetMapping("/create")
    public String create(Model model) {
        Product product = new Product();
        model.addAttribute("product", product);
        return "form";
    }

    @PostMapping("/save")
    public String save(@Valid Product product) {
        productService.save(product);
        return "redirect:/list";
    }

    @GetMapping("/edit/{idproducts}")
    public String edit(@PathVariable(name = "idproducts") int idproducts, Model model) {
        Product product = null;
        if (0 < idproducts) {
            product = productService.findOne(idproducts);
        } else {
            return "redirect:/list";
        }
        model.addAttribute("product", product);
        return "form";
    }

    @GetMapping("/delete/{idproducts}")
    public String delete(@PathVariable(name = "idproducts") int idproducts, Model model){
        productService.delete(idproducts);
        return "redirect:/list";
    }
}

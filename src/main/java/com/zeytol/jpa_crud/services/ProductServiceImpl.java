package com.zeytol.jpa_crud.services;

import com.zeytol.jpa_crud.models.Product;
import com.zeytol.jpa_crud.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService{
    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    @Override
    public void save(Product product) {
        productRepository.save(product);
    }

    @Override
    public Product findOne(int id) {
        return productRepository.findById(id).get();
    }

    @Override
    public void delete(int id) {
        productRepository.deleteById(id);
    }
}

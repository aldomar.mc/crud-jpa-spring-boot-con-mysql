package com.zeytol.jpa_crud.services;

import com.zeytol.jpa_crud.models.Product;

import java.util.List;

public interface ProductService {
    public List<Product> getAll();

    public void save(Product product);

    public Product findOne(int id);

    public void delete(int id);
}

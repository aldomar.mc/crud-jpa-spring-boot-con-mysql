#CRUD JPA con Spring Boot y MySQL

#Herramientas utilizadas
Se desarrollo con el framework spring boot, utilizando el motor de plantillas thymeleaf. El CRUD esta hecho con JPA.
El motor de base de datos usado es MySQL v5.5, Java v11, programada en Intellij idea

#Nota
En el archivo files se encuentra el backup de la base de datos

#Contacto
Aldo Omar: aomc@outlook.es
